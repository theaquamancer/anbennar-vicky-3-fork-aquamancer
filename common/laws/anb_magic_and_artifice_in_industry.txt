﻿law_mundane_production = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_amoral_artifice_banned
		law_dark_arts_banned
	}
	
	modifier = {
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	#Will only unban one at a time
	possible_political_movements = {
		law_traditional_magic_only
		law_artifice_only
	}
}

law_traditional_magic_only = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_amoral_artifice_banned
	}
	
	modifier = {
		country_mages_pol_str_mult = 0.5
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_traditional_magic_encouraged
	}

	pop_support = {
		value = 0
		
		add = {
			desc = "POP_MAGES"
			if = {
				limit = { 
					is_pop_type = mages
				}
				value = 0.5
			}
		}
	}

	ai_will_do = {
		exists = ruler
		ruler = {
			has_magocrat_ideology = yes
		}
	}

	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_traditional_magic_encouraged = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_mages_pol_str_mult = 0.2
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_traditional_magic_only
		law_artifice_encouraged
	}

	pop_support = {
		value = 0
		
		add = {
			desc = "POP_MAGES"
			if = {
				limit = { 
					is_pop_type = mages
				}
				value = 0.25
			}
		}
	}

	ai_will_do = {
		exists = ruler
		ruler = {
			has_magocrat_ideology = yes
		}
	}

	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_artifice_encouraged = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
	}
	
	possible_political_movements = {
		law_traditional_magic_encouraged
		law_artifice_only
	}

	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_artifice_only = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_dark_arts_banned
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
	}
	
	possible_political_movements = {
		law_artifice_encouraged
	}

	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}