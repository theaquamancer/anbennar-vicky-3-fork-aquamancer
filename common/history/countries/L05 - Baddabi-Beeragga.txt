﻿COUNTRIES = {
	c:L05 ?= {
		effect_starting_technology_baashidi_tech = yes

		effect_starting_politics_conservative = yes # INVALID - All baashidi are invalid cause of conservative + baashidi_tech, could be traditional instead

		activate_law = law_type:law_autocracy
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_frontier_colonization
		activate_law = law_type:law_no_police
		activate_law = law_type:law_poor_laws
		activate_law = law_type:law_slave_trade
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_poor_laws
		
	}
}