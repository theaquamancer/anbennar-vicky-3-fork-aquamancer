﻿COUNTRIES = {
	c:L12 ?= {
		effect_starting_technology_tier_3_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_no_police
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_mundane_production

	}
}