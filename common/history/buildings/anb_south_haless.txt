﻿BUILDINGS={
	s:STATE_QIANZHAOLIN = {
		region_state:R13 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					country={
						country="c:R13"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=1
						region="STATE_QIANZHAOLIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=3
						region="STATE_QIANZHAOLIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_JIEZHONG = {
		region_state:R13 = {
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:R13"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_arms_industry"
				add_ownership={
					country={
						country="c:R13"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_muskets" }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=4
						region="STATE_JIEZHONG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=4
						region="STATE_JIEZHONG"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=2
						region="STATE_JIEZHONG"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:R13"
						levels=1
						region="STATE_JIEZHONG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" }
			}
		}
	}

	s:STATE_WANGQIU = {
		region_state:R13 = {
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:R13"
						levels=1
						region="STATE_WANGQIU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=2
						region="STATE_WANGQIU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:R13"
						levels=1
						region="STATE_WANGQIU"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:R13"
						levels=4
						region="STATE_WANGQIU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" }
			}
		}
	}

	s:STATE_SIKAI = {
		region_state:Y17 = {
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y17"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
	}

	s:STATE_NAGON = {
		region_state:Y19 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y19"
						levels=1
						region="STATE_NAGON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" }
			}
		}
		region_state:Y32 = {
			create_building={
				building="building_opium_plantation"
				add_ownership={
					building={
						type="building_opium_plantation"
						country="c:Y32"
						levels=15
						region="STATE_NAGON"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y32"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
	}

	s:STATE_KHINDI = {
		region_state:Y32 = {
			create_building={
				building="building_sugar_plantation"
				add_ownership={
					building={
						type="building_sugar_plantation"
						country="c:Y32"
						levels=10
						region="STATE_KHINDI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_tea_plantation"
						country="c:Y32"
						levels=1
						region="STATE_KHINDI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_opium_plantation"
				add_ownership={
					building={
						type="building_opium_plantation"
						country="c:Y32"
						levels=15
						region="STATE_KHINDI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y32"
						levels=4
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
	}

	s:STATE_LOWER_TELEBEI = {
		region_state:Y32 = {
			create_building={
				building="building_cotton_plantation"
				add_ownership={
					building={
						type="building_cotton_plantation"
						country="c:Y32"
						levels=4
						region="STATE_LOWER_TELEBEI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_silk_plantation"
						country="c:Y32"
						levels=4
						region="STATE_LOWER_TELEBEI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_textile_mills"
						country="c:Y32"
						levels=5
						region="STATE_LOWER_TELEBEI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:Y32"
						levels=2
						region="STATE_LOWER_TELEBEI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y32"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y32"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_BIM_LAU = {
		region_state:Y32 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y32"
						levels=1
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
		}
		region_state:Y15 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:Y15"
						levels=1
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_open_air_stockyards" "pm_standard_fences" "pm_unrefrigerated" }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:Y15"
						levels=1
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
		}
		region_state:Y14 = {
			create_building={
				building="building_cotton_plantation"
				add_ownership={
					building={
						type="building_cotton_plantation"
						country="c:Y14"
						levels=6
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_silk_plantation"
						country="c:Y14"
						levels=5
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_glassworks"
				add_ownership={
					building={
						type="building_glassworks"
						country="c:Y14"
						levels=3
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_arms_industry"
				add_ownership={
					building={
						type="building_arms_industry"
						country="c:Y14"
						levels=2
						region="STATE_BIM_LAU"
					}
				}
				reserves=1
				activate_production_methods={ "pm_muskets" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y14"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
	}

	s:STATE_KHARUNYANA_BOMDAN = {
		region_state:Y32 = {
			create_building={
				building="building_dye_plantation"
				add_ownership={
					building={
						type="building_dye_plantation"
						country="c:Y32"
						levels=5
						region="STATE_KHARUNYANA_BOMDAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_tea_plantation"
						country="c:Y32"
						levels=15
						region="STATE_KHARUNYANA_BOMDAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_glassworks"
				add_ownership={
					building={
						type="building_glassworks"
						country="c:Y32"
						levels=2
						region="STATE_KHARUNYANA_BOMDAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y32"
						levels=4
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
		region_state:Y13 = {
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_tea_plantation"
						country="c:Y13"
						levels=10
						region="STATE_KHARUNYANA_BOMDAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:Y13"
						levels=1
						region="STATE_KHARUNYANA_BOMDAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y13"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
	}

	s:STATE_486 = {
		region_state:Y32 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y32"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_KHABTEI_TELENI = {
		region_state:Y16 = {
			create_building={
				building="building_opium_plantation"
				add_ownership={
					building={
						type="building_opium_plantation"
						country="c:Y16"
						levels=5
						region="STATE_KHABTEI_TELENI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_textile_mills"
						country="c:Y16"
						levels=1
						region="STATE_KHABTEI_TELENI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y16"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
		}
	}

	s:STATE_HOANGDESINH = {
		region_state:A09 = {
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=8
						region="STATE_HOANGDESINH"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A09"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:Y19 = {
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=2
						region="STATE_HOANGDESINH"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y19"
						levels=1
						region="STATE_HOANGDESINH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:Y19"
						levels=2
						region="STATE_HOANGDESINH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_shipyards"
				add_ownership={
					building={
						type="building_shipyards"
						country="c:Y19"
						levels=1
						region="STATE_HOANGDESINH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y19"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y19"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_KHOM_MA = {
		region_state:Y19 = {
			create_building={
				building="building_banana_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=3
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=4
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=2
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_textile_mills"
						country="c:Y19"
						levels=1
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" }
			}
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_furniture_manufacturies"
						country="c:Y19"
						levels=1
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_luxury_furniture" "pm_automation_disabled" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_tooling_workshops"
						country="c:Y19"
						levels=1
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods = { "pm_crude_tools" "pm_automation_disabled" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:Y19"
						levels=2
						region="STATE_KHOM_MA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y19"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}

	s:STATE_PHONAN = {
		region_state:Y19 = {
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:Y19"
						levels=4
						region="STATE_PHONAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=1
						region="STATE_PHONAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_open_air_stockyards" "pm_standard_fences" "pm_unrefrigerated" }
			}
			create_building={
				building="building_cotton_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=1
						region="STATE_PHONAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_sugar_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y19"
						levels=1
						region="STATE_PHONAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_RONGBEK = {
		region_state:Y18 = {
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y18"
						levels=2
						region="STATE_RONGBEK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y18"
						levels=1
						region="STATE_RONGBEK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_open_air_stockyards" "pm_standard_fences" "pm_unrefrigerated" }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y18"
						levels=1
						region="STATE_RONGBEK"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_KUDET_KAI = {
		region_state:Y21 = {
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y21"
						levels=2
						region="STATE_KUDET_KAI"
					}
					building={
						type="building_manor_house"
						country="c:A09"
						levels=1
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:Y21"
						levels=1
						region="STATE_KUDET_KAI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y21"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
		region_state:Y33 = {
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:A09"
						levels=5
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:A09"
						levels=7
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_open_air_stockyards" "pm_standard_fences" "pm_unrefrigerated" }
			}
			create_building={
				building="building_coffee_plantation"
				add_ownership={
					building={
						type="building_coffee_plantation"
						country="c:Y33"
						levels=5
						region="STATE_KUDET_KAI"
					}
					building={
						type="building_coffee_plantation"
						country="c:A09"
						levels=10
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y33"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}

	s:STATE_SIRTAN = {
		region_state:Y22 = {
		}
	}

	s:STATE_TLAGUKIT = {
		region_state:Y24 = {
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:Y24"
						levels=1
						region="STATE_TLAGUKIT"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_tea_plantation"
						country="c:Y24"
						levels=1
						region="STATE_TLAGUKIT"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_REWIRANG = {
		region_state:Y25 = {
		}
	}

	s:STATE_MESATULEK = {
		region_state:Y30 = {
		}
		region_state:Y31 = {
		}
		region_state:Y33 = {
			create_building={
				building="building_banana_plantation"
				add_ownership={
					building={
						type="building_banana_plantation"
						country="c:A09"
						levels=5
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A09"
						levels=1
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:Y33"
						levels=2
						region="STATE_MESATULEK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y33"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y33"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:Y33"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
		region_state:Y23 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y23"
						levels=1
						region="STATE_MESATULEK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:Y23"
						levels=1
						region="STATE_MESATULEK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
		}
	}

	s:STATE_YEMAKAIBO = {
		region_state:Y33 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A09"
						levels=1
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:Y33"
						levels=1
						region="STATE_YEMAKAIBO"
					}
				}
				reserves=1
				activate_production_methods={ "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
		}
	}

	s:STATE_BINHRUNGHIN = {
		region_state:Y33 = {
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=10
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_dye_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=10
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=2
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y33"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}

	s:STATE_NON_CHIEN = {
		region_state:Y33 = {
			create_building={
				building="building_sugar_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=5
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=2
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_coffee_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=2
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A09"
						levels=1
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
		}
	}

	s:STATE_487 = {
		region_state:Y33 = {
			create_building={
				building="building_banana_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=5
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:Y33"
						levels=1
						region="STATE_487"
					}
				}
				reserves=1
				activate_production_methods={ "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y33"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_490 = {
		region_state:Y33 = {
			create_building={
				building="building_tobacco_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A09"
						levels=3
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y33"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_ARAWKELIN = {
		region_state:Y23 = {
			create_building={
				building="building_banana_plantation"
				add_ownership={
					building={
						type="building_banana_plantation"
						country="c:Y23"
						levels=5
						region="STATE_ARAWKELIN"
					}
					building={
						type="building_manor_house"
						country="c:A09"
						levels50
						region="STATE_HAPAINE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:Y23"
						levels=5
						region="STATE_ARAWKELIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_fig_orchards_building_rice_farm" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:Y23"
						levels=1
						region="STATE_ARAWKELIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_open_air_stockyards" "pm_standard_fences" "pm_unrefrigerated" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:Y23"
						levels=2
						region="STATE_ARAWKELIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:Y23"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}

	s:STATE_488 = {
		region_state:Y34 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:Y34"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

}