﻿BUILDINGS={
	s:STATE_BALMIRE={ #Tis a marsh
		region_state:A04={
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A04"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A04"
						levels=2
						region="STATE_BALMIRE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A04"
						levels=1
						region="STATE_BALMIRE"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_ADENICA = {
		region_state:A22 = {
			create_building={ #Adenican livestock
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=3
						region="STATE_ADENICA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=2
						region="STATE_ADENICA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=1
						region="STATE_ADENICA"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={ # For their guns, not enough to cover their needs tho so they'll have to import
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A22"
						levels=1
						region="STATE_ADENICA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_increased_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
		}
	}

	s:STATE_ROHIBON = {
		region_state:A22 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=3
						region="STATE_ROHIBON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=1
						region="STATE_ROHIBON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A22"
						levels=5
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
		}
	}

	s:STATE_ANCARDIAN_PLAINS = {
		region_state:A22 = {
			create_building={ #Very basic adminstration
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A22"
						levels=7
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
			create_building={ # Make em more gunny
				building="building_arms_industry"
				add_ownership={
					country={
						country="c:A22"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_muskets" }
			}
			create_building={
				building="building_munition_plants"
				add_ownership={
					country={
						country="c:A22"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_percussion_caps" }
			}
			create_building={
				building="building_food_industry"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A22"
						levels=2
						region="STATE_ANCARDIAN_PLAINS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={ #For military use only, pls do not use for making civilian factories ty
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:A22"
						levels=1
					}
				}
				reserves=1
				activate_production_methods = { "pm_wooden_buildings" }
			}
			create_building={ # Halflings work there
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=1
						region="STATE_ANCARDIAN_PLAINS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
		}
	}

	s:STATE_FARRANEAN = {
		region_state:A22 = {
		}
	}

	s:STATE_MORECED = {
		region_state:A22 = {
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A22"
						levels=3
						region="STATE_MORECED"
					}
				}
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
		}
	}

	s:STATE_NEWSHIRE = { #Farms and food
		region_state:A24 = {
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A24"
						levels=8
						region="STATE_NEWSHIRE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A24"
						levels=1
						region="STATE_NEWSHIRE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

	s:STATE_OUDMERE = {
		region_state:A25 = {
			create_building={ # They're traders really, so some minor paper industry for their admin but little else
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:A25"
						levels=1
						region="STATE_OUDMERE"
					}
				}
				reserves=1
				activate_production_methods = { "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A25"
						levels=1
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A25"
						levels=3
						region="STATE_OUDMERE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools" } 
			}
			create_building={ #they make fancy trader wines
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A25"
						levels=2
						region="STATE_OUDMERE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A25"
						levels=2
						region="STATE_OUDMERE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A25"
						levels=2
						region="STATE_OUDMERE"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

	s:STATE_DEVACED = {
		region_state:A27 = {
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=1
						region="STATE_DEVACED"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=1
						region="STATE_DEVACED"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=2
						region="STATE_DEVACED"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=3
						region="STATE_DEVACED"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=1
						region="STATE_DEVACED"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
		}
	}

	s:STATE_VERNHAM = {
		region_state:A27 = {
			create_building={ # Better adminstration cause they're not a separatist
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A27"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:A27"
						levels=1
					}
				}
				reserves=1
				activate_production_methods = { "pm_wooden_buildings" }
			}
			create_building={ #Lots of food for a big army
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=3
						region="STATE_VERNHAM"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=1
						region="STATE_VERNHAM"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=2
						region="STATE_VERNHAM"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" }
			}
			create_building={
				building="building_arms_industry"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=1
						region="STATE_VERNHAM"
					}
				}
				reserves=1
				activate_production_methods = { "pm_muskets" }
			}
			create_building={ #Old General academy
				building="building_university"
				add_ownership={
					country={
						country="c:A27"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={ #Bladedancing stuff, cause that's retro now
				building="building_arts_academy"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=1
						region="STATE_VERNHAM"
					}
				}
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:A27"
						levels=1
						region="STATE_VERNHAM"
					}
				}
				reserves=1
				activate_production_methods = { "pm_pulp_pressing" "pm_automation_disabled" }
			}
		}
	}

	s:STATE_DOSTANS_WAY = {
		region_state:A27 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=2
						region="STATE_DOSTANS_WAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=2
						region="STATE_DOSTANS_WAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=1
						region="STATE_DOSTANS_WAY"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A27"
						levels=1
						region="STATE_DOSTANS_WAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_BLADEMARCH = {
		region_state:A27 = {
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A27"
						levels=3
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
			create_building={ # Make furniture from their wood I guess
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A27"
						levels=1
						region="STATE_BLADEMARCH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_luxury_furniture" "pm_automation_disabled" }
			}
		}
	}

	s:STATE_SILVERVORD = {
		region_state:A26 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A26"
						levels=3
						region="STATE_SILVERVORD"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_CANNWOOD = {
		region_state:A26 = {
			create_building={
				building="building_manor_house"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:A26"
						levels=1
						region="STATE_CANNWOOD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_MIDDLE_ALEN = {
		region_state:A10={
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_furniture_manufacturies"
						country="c:A10"
						levels=2
						region="STATE_MIDDLE_ALEN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_lathes" "pm_automation_disabled" "pm_luxury_furniture" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A10"
						levels=4
						region="STATE_MIDDLE_ALEN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A10"
						levels=8
						region="STATE_MIDDLE_ALEN"
					}
				}
				reserves=1
				activate_production_methods={  "pm_simple_farming" "pm_apple_orchards" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_EBONMARCK = { #Tis a forest
		region_state:A10={
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A10"
						levels=3
						region="STATE_EBONMARCK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:A10"
						levels=6
						region="STATE_EBONMARCK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_BURNOLL = { #No clue what these guys are about so I've left em with just a barracks and a farm. Maybe they're meant to be food
		region_state:A32={
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A32"
						levels=1
						region="STATE_BURNOLL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools_disabled" } 
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A32"
						levels=1
						region="STATE_BURNOLL"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
		region_state:A28={
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A28"
						levels=2
						region="STATE_BURNOLL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools_disabled" }
			}
			create_building={ # They make fancy Rosander wines
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A28"
						levels=2
						region="STATE_BURNOLL"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_ROSANVORD = {
		region_state:A28={
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A28"
						levels=5
						region="STATE_ROSANVORD"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A28"
						levels=3
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
		}
	}

	s:STATE_MEDIRLEIGH = {
		region_state:A28={
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A28"
						levels=1
						region="STATE_MEDIRLEIGH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A28"
						levels=4
						region="STATE_MEDIRLEIGH"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A28"
						levels=2
						region="STATE_MEDIRLEIGH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_HORNWOOD = {
		region_state:A29={
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=4
						region="STATE_HORNWOOD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_citrus_orchards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A29"
						levels=1
						region="STATE_HORNWOOD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=3
						region="STATE_HORNWOOD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_DRYADSDALE = {
		region_state:A29={
			create_building={ # They do some industry stuff in their MT, this makes more sense than giving them steel
				building="building_food_industry"
				add_ownership={
					building={
						type="building_food_industry"
						country="c:A29"
						levels=1
						region="STATE_DRYADSDALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_manual_dough_processing" "pm_pot_stills" }
			}
			create_building={
				building="building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=2
						region="STATE_DRYADSDALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=1
						region="STATE_DRYADSDALE"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_MARRVALE = {
		region_state:A29={
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A29"
						levels=1
						region="STATE_MARRVALE"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_arms_industry"
				add_ownership={
					building={
						type="building_arms_industry"
						country="c:A29"
						levels=1
						region="STATE_MARRVALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_muskets" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=3
						region="STATE_MARRVALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={ #Griffon meat???
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=4
						region="STATE_MARRVALE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A29"
						levels=3
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
		}
	}

	s:STATE_ESSHYL = {
		region_state:A29={
			create_building={ #Not a core territory so they had to make admin here to legitimize it
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A29"
						levels=2
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_organization" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A29"
						levels=2
						region="STATE_ESSHYL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:A29"
						levels=2
						region="STATE_ESSHYL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_tools_disabled" "pm_apple_orchards" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A29"
						levels=2
						region="STATE_ESSHYL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_UNGULDAVOR = { #Doc says they don't utilize their land well
		region_state:A31={
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A31"
						levels=1
						region="STATE_UNGULDAVOR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A31"
						levels=1
						region="STATE_UNGULDAVOR"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A31"
						levels=1
						region="STATE_UNGULDAVOR"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_KONDUNN = { #Doc says they don't utilize their land well
		region_state:A31={
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A31"
						levels=1
						region="STATE_KONDUNN"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_CASTONATH = { 
		region_state:A30={
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A30"
						levels=4
						region="STATE_CASTONATH"
					}
				}
				reserves=1
				activate_production_methods = { "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A30"
						levels=1
						region="STATE_CASTONATH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" }
			}
			create_building={ #Ex-heart of Escann
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A30"
						levels=5
					}
				}
				reserves=1
				activate_production_methods = { "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_glassworks"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A30"
						levels=1
						region="STATE_CASTONATH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_disabled_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_food_industry"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A30"
						levels=3
						region="STATE_CASTONATH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_bakery" "pm_disabled_canning" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=3
						region="STATE_CASTONATH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_tools_disabled" "pm_apple_orchards" }
			}
		}
	}

	s:STATE_LOWER_NATH = { #Esthil builds farms here
		region_state:A30={
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=6
						region="STATE_LOWER_NATH"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=1
						region="STATE_LOWER_NATH"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_WESTGATE = { 
		region_state:A30={
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=2
						region="STATE_WESTGATE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_tools_disabled" "pm_apple_orchards" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A30"
						levels=1
					}
				}
				reserves=1
				activate_production_methods = { "pm_horizontal_drawer_cabinets" }
			}
		}
	}

	s:STATE_NORTHYL = { 
		region_state:A30={
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A30"
						levels=2
						region="STATE_NORTHYL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=2
						region="STATE_NORTHYL"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_SERPENTSMARCK = { 
		region_state:A30={
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=1
						region="STATE_SERPENTSMARCK"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_STEELHYL = {
		region_state:A30={
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:A30"
						levels=2
						region="STATE_STEELHYL"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={ #Where the guns used to be made
				building="building_arms_industry"
				add_ownership={
					building={
						type="building_arms_industry"
						country="c:A30"
						levels=2
						region="STATE_STEELHYL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_muskets" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=1
						region="STATE_STEELHYL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_EASTGATE = { 
		region_state:A30={
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A30"
						levels=2
						region="STATE_EASTGATE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=3
						region="STATE_EASTGATE"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:A30"
						levels=1
					}
				}
				reserves=1
				activate_production_methods = { "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=2
						region="STATE_EASTGATE"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_UPPER_NATH = { 
		region_state:A30={
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:A30"
						levels=3
						region="STATE_UPPER_NATH"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={ #Corintar has stuff for logging here
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:A30"
						levels=3
						region="STATE_UPPER_NATH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
		}
	}

}