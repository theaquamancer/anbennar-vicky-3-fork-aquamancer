﻿MILITARY_FORMATIONS = {
	c:E01 ?= { #Kalsyto
		create_military_formation = {
			type = army
			hq_region = sr:region_triunic_lakes
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SHIK_DAZAR
				count = 3
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ZOI_KHORKHIIN
				count = 2
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SHIKENKHIIN
				count = 3
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SARTZ_NEISAR
				count = 3
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KESH_GOLKHIN
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_QUSHYIL
				count = 3
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_YUKAROYOL
				count = 2
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_YUKARON
				count = 4
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_PRIKOYOL
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GADHLUMO
				count = 6
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_PEENADHI
				count = 4
			}
		}
		create_military_formation = {
			type = fleet
			hq_region = sr:region_triunic_lakes
			
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_SHIK_DAZAR
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SHIKENKHIIN
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SARTZ_NEISAR
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_KESH_GOLKHIN
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_QUSHYIL
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_YUKARON
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_PRIKOYOL
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_GADHLUMO
				count = 1
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_PEENADHI
				count = 2
			}
		}
	}
	c:E02 ?= { #Magharma
		create_military_formation = {
			type = army
			hq_region = sr:region_north_plains
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_FOIRAKHIAN
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SHEVROMRZGH
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SKURKHA_KYARD
				count = 10
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_OVTO_KIGVAL
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KVAKEINOLBA
				count = 5
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DZIMOKLI
				count = 5
			}
		}
	}
	c:E03 ?= { #Zabutodask
		create_military_formation = {
			type = army
			hq_region = sr:region_north_plains
			
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SOCHULEAG
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SOCHULEAG
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ADOI_FILEANAN
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_ADOI_FILEANAN
				count = 4
			}
		}
	}
	c:E04 ?= { #Shivusdoyen
		create_military_formation = {
			type = army
			hq_region = sr:region_south_plains
			 
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ORCHEKH
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_ORCHEKH
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_APORLAEN
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_APORLAEN
				count = 4
			}
		}
	}
	c:E05 ?= { #River Centaur
		create_military_formation = {
			type = army
			hq_region = sr:region_south_plains
			
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SERPENT_GIFT
				count = 9
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SERPENT_GIFT
				count = 6
			}
		}
	}
	c:E06 ?= { #West Desert Centaur
		create_military_formation = {
			type = army
			hq_region = sr:region_south_plains
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_CAUBHEAMEAS
				count = 5
			}
		}
	}
	c:E07 ?= { #East Desert Centaur
		create_military_formation = {
			type = army
			hq_region = sr:region_south_plains
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_IRDU_AGEENEAS
				count = 5
			}
		}
	}
	c:E08 ?= { #West Steppe Centaur
		create_military_formation = {
			type = army
			hq_region = sr:region_north_plains
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_UGHEABAR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_UGHEABAR
				count = 2
			}
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NAGLAIBAR
				count = 3
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_NAGLAIBAR
				count = 2
			}
		}
	}
	c:E09 ?= { #East Steppe Centaur
		create_military_formation = {
			type = army
			hq_region = sr:region_south_plains
			
			combat_unit = { 
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GHANEERSP
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_GHANEERSP
				count = 4
			}
		}
	}
}