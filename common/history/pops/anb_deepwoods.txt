﻿POPS = {
	s:STATE_LAKE_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 1181100
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 88900
			}
		}
	}
	s:STATE_MOUNTAIN_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 916400
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 45240
				religion = corinite
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 52780
				religion = old_dookan
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 52780
				religion = bulgu_orazan
			}
			create_pop = {
				culture = emerald_orc
				size = 92800
			}
		}
	}
	s:STATE_THORN_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 516800
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 61200
			}
			create_pop = {
				culture = emerald_orc
				size = 102000
			}
		}
	}
	s:STATE_HUNTER_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 655700
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 22410
				religion = corinite 
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 26145
				religion = old_dookan
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 26145
				religion = bulgu_orazan
			}
			create_pop = {
				culture = emerald_orc
				size = 99600
			}
		}
	}
	s:STATE_FLOWER_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 691260
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = corinite
				size = 11316
			}
      create_pop = {
				culture = karakhanbari_orc
				size = 13202
				religion = bulgu_orazan
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 13202
				religion = old_dookan
			}
			create_pop = {
				culture = emerald_orc
				size = 118020
			}
		}
	}
	s:STATE_RIVER_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 260320
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = old_dookan
				size = 11696
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = bulgu_orazan
				size = 631696
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 181120
			}
		}
	}
	s:STATE_SHADOW_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 320500
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 496800
				religion = bulgu_orazan
			}
			create_pop = {
				culture = firanyan_harpy
				size = 26180
			}
		}
	}
	s:STATE_ARROW_GROVE = {
		region_state:A80 = {
			create_pop = {
				culture = wood_elven
				size = 7740
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 94041
				religion = bulgu_orazan
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = corinite
				size = 31347
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = old_dookan
				size = 83592
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 557280
			}
		}
	}
	s:STATE_BONE_GROVE = {
		region_state:A80 = {
			create_pop = {
				culture = wood_elven
				size = 19640
			}
			create_pop = {
				    culture = karakhanbari_orc
				    size = 88380
				    religion = old_dookan
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = corinite
				size = 29460
			}
			create_pop = {
				culture = karakhanbari_orc
				religion = bulgu_orazan
				size = 78560
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 765960
			}
		}
	}
}