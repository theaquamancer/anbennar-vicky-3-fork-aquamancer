﻿testorian_heritage = {	#Do not delete. Used to invalidate some stuff

}

cannorian_heritage = {
	heritage = yes
}

bulwari_heritage = {
	heritage = yes
}

divenori_heritage = {
	heritage = yes
}

raheni_heritage = {
	heritage = yes
}

northeast_halessi_heritage = {
	heritage = yes
}

southeast_halessi_heritage = {
	heritage = yes
}

serpentspine_heritage = {
	heritage = yes
}

serpentdepths_heritage = {
	heritage = yes
}

fp_heritage = {
	heritage = yes
}

sarhaly_heritage = {	#may need to be broken up eventually
	heritage = yes
}

south_sarhaly_heritage = {
	heritage = yes
}

west_sarhaly_heritage = {
	heritage = yes
}

aelantiri_heritage = {
	heritage = yes
}

troll_heritage = {
	heritage = yes
}

mengi_heritage = {
	heritage = yes
}

northern_pass_heritage = {
	heritage = yes
}