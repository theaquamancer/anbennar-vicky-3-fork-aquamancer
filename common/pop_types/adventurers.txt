﻿#Lower strata pop that can accumulate wealth quickly without need for much qualifications
#Not politically active, until they are rich and then they are
#Also demands high quality of life

adventurers = {
	texture = "gfx/interface/icons/pops_icons/adventurers.dds"
	color = hsv{ 0.8 0.87 0.86 }
	strata = middle
	start_quality_of_life = 5
	can_always_hire = yes
	wage_weight = 5	#same level as aristocrats, they care a lot about their wage. This increases how much wages they are paid btw
	paid_private_wage = yes
	literacy_target = 0.5
	dependent_wage = 0.0	# per year, no dependents
	unemployment = yes
	
	# 50% - 100% politically engaged	
	political_engagement_base = 0.25
	political_engagement_literacy_factor = 1.0

	#Future, make soldiers more likely to become adventurers

	#Can upgrade into capitalists, aristocrats, soldiers, officers, engineers


	#A lot of custom ones here


	political_engagement_mult = {
		value = 1

		add = {
			desc = "POP_DECADENT_ADVENTURERS"	
			
			if = {
				limit = { 
					standard_of_living > 15	#if middling or above
				}
				value = 1
			}
		}	

		add = {
			desc = "POP_VERY_DECADENT_ADVENTURERS"	
			
			if = {
				limit = { 
					standard_of_living > 30	#if affluent
				}
				value = 1
			}
		}	
		
		add = {
			desc = "POP_STARVATION"	
			
			if = {
				limit = { 
					standard_of_living < 5
				}
				value = 1
			}		
		}

		multiply = {	#they go mental here
			desc = "HAS_POLITICAL_AGITATION"	
			value = 1.0
			
			if = {
				limit = { 
					owner = {
						has_technology_researched = political_agitation
					}
				}
				value = 3	#base is 3
			}		
		}		
	}
	
	portrait_age = {
		integer_range = {
			min = define:NPortrait|GRACEFUL_AGING_START
			max = 40
		}
	}
	portrait_pose = { value = 0 }			
	portrait_is_female = { always = yes }
}