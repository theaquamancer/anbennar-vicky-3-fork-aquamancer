﻿A03 = { # Lorent
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A05 = { # Bisan
	states = {
		STATE_BISAN
	}
	
	ai_will_do = { always = no }
}

A06 = { # Gnomish Hierarchy
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A07 = { #Reverian
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A08 = { #Eborthil
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A09 = { #Busilar
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
} 

A10 = { #Grombar
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
} 

A11 = { #Deshak
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A12 = { #Khasa
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A13 = { #Ekha
	states = {
		STATE_EKHA
	}
	
	ai_will_do = { always = no }
}

A14 = { #Small Country
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A15 = { #Portnamm
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A16 = { #Kobildzan
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A18 = { #Bayvek
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A19 = { #Vertesk
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A20 = { # Ibevar
	states = {
		STATE_NORTH_IBEVAR
		STATE_SOUTH_IBEVAR
		STATE_CURSEWOOD
		STATE_FARRANEAN
	}
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A21 = { # Farranean
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A22 = { # Ancardia
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A23 = { # Elikhand
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A24 = { # Newshire
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A25 = { # Araionn
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A26 = { # Wyvernheart
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A27 = { # Blademarches
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A28 = { # Rosande
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A29 = { # Marrhold
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A30 = { # Magocratic Demesne / Nurcestir?
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A31 = { # Unguldavor
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A32 = { # Silvermere
	states = {
		STATE_BURNOLL
	}
	
	ai_will_do = { always = no }
}

A33 = { # Ravelian Rectorate
	states = {
		STATE_PASHAINE #???
	}
	
	ai_will_do = { always = no }
}

A34 = { # Telgeir
	states = {
		STATE_TELGEIR_CALASCANDAR
	}
	
	ai_will_do = { always = no }
}

A35 = { # Konwell
	states = {
		STATE_KONWELL
	}
	
	ai_will_do = { always = no }
}

A36 = { # Estallen
	states = {
		STATE_ESTALLEN
	}
	
	ai_will_do = { always = no }
}

A37 = { # Silverforge
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A38 = { # Bennon
	states = {
		STATE_BENNON
	}
	
	ai_will_do = { always = no }
}

A39 = { # Leslinpar
	states = {
		STATE_HIGH_ESMAR
	}
	
	ai_will_do = { always = no }
}

A40 = { # Esmaraine
	states = {
		STATE_CANN_ESMAR
	}
	
	ai_will_do = { always = no }
}

A41 = { # Wex
	use_culture_states = yes
	
	required_num_states = 2
	
	ai_will_do = { always = no }
}

A44 = { # Moonhaven
	states = {
		STATE_MOONHAVEN
	}
	
	ai_will_do = { always = no }
}

A45 = { # Pearsledge
	use_culture_states = yes
		
	ai_will_do = { always = no }
}

A46 = { # Neckcliffe
	states = {
		STATE_NECKCLIFFE
	}
	
	ai_will_do = { always = no }
}

A48 = { # Verne
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A49 = { # Carneter
	states = {
		STATE_NECKCLIFFE
		STATE_WESDAM
		STATE_ROILSARD
		STATE_PEARLSEDGE
		STATE_CARNETER
	}
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A50 = { # Wesdam
	states = {
		STATE_WESDAM
	}

	ai_will_do = { always = no }
}

A51 = { # Istralore
	states = {
		STATE_ISTRALORIAN_DALES
	}
	
	ai_will_do = { always = no }
}

A55 = { # Pashaine
	states = {
		STATE_PASHAINE
	}
	
	ai_will_do = { always = no }
}

A57 = { # Dameria
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A58 = { # Gawed
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A59 = { # Westmoors
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A60 = { # Alencay
	states = {
		STATE_REACHSPIER
	}

	ai_will_do = { always = no }
}

A61 = { # Adshaw
	states = {
		STATE_ADSHAW
	}

	ai_will_do = { always = no }
}

A62 = { # Bjarnrik
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A63 = { # Obrtrol
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A64 = { # Olavlund
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A65 = { # Redglades
	states = {
		STATE_REDGLADES
	}
	ai_will_do = { always = no }
}

A66 = { # Corvuria
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A67 = { #Arannen
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A68 = { # Arbaran
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A69 = { # Esmaria
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A71 = { # Honderaak
	use_culture_states = yes
	
	#required_num_states = 2

	ai_will_do = { always = no }
}

A72 = { # Celmaldor
	states = {
		STATE_SERPENTSHEAD
	}

	ai_will_do = { always = no }
}

A73 = { # Sorncost
	use_culture_states = yes
	
	required_num_states = 2

	ai_will_do = { always = no }
}

A74 = { # Roilsard
	use_culture_states = yes
	
	ai_will_do = { always = no }
}

A78 = { # Deranne
	use_culture_states = yes

	required_num_states = 1 #Deranne proper is always enough (as they have a lot of small island homelands)
	
	ai_will_do = { always = no }
}

B19 = { # Cestirmark
	states = {
		STATE_CESTIRMARK
		STATE_TROLLSBRIDGE
	}

	ai_will_do = { always = no }
}

B20 = { # Marlliande
	states = {
		STATE_NORTH_MARLLIANDE
		STATE_SOUTH_MARLLIANDE
	}

	ai_will_do = { always = no }
}

B22 = { # Zanlib
	use_culture_states = yes

	ai_will_do = { always = no }
}

B23 = { # Isobelin
	states = {
		STATE_ISOBELIN
	}

	ai_will_do = { always = no }
}

B24 = { # Thilvis
	use_culture_states = yes

	ai_will_do = { always = no }
}

B25 = { # Valorpoint
	states = {
		STATE_VALORPOINT
	}

	ai_will_do = { always = no }
}

B48 = { # Ebenmas
	states = {
		STATE_EBENMAS
		STATE_EKRSOKA
	}

	ai_will_do = { always = no }
}

B44 = { # Tellumtir
	states = {
		STATE_TELLUMTIR
		STATE_UZOO
	}

	ai_will_do = { always = no }
}

B92 = { #Sornicande
	states = {
		STATE_SORNICANDE
		STATE_SOUTH_DALAIRE
	}

	#required_num_states = 2
		
	ai_will_do = { always = no }
}

B93 = { #Kwinelliande
	states = {
		STATE_KWINELLIANDE
	}
		
	ai_will_do = { always = no }
}

B94 = { #Spoorland
	states = {
		STATE_SPOORLAND STATE_BOEK
	}
		
	ai_will_do = { always = no }
}

B97 = { #Dolindha
	states = {
		STATE_ROGAIDHA STATE_VITREYNN STATE_USLAD STATE_MOCEPED STATE_ARGANJUZORN STATE_BRELAR
	}

	required_num_states = 4
		
	ai_will_do = { always = no }
}
